// Require the framework and instantiate it
const fastify = require('fastify')()
const cors = require('cors')
const path = require('path')
var serveStatic = require('serve-static')

fastify.use(serveStatic(`${__dirname}/public`))


fastify.use(cors())
var knex = require('knex')({
    client: 'pg',
    connection: {
        host : '127.0.0.1',
        user : 'postgres',
        password : 'postgres',
        database : 'temp'
    }
});
// Declare a route
fastify.get('/', async function (req, reply) {
    reply.sendFile('index.html')
})

fastify.get('/:span', async (req, res)=>{
    let data  = await knex
                    .from('templog')
                    .select('date','temp')
    console.log(data);
    return data;
})

// Run the server!
const start = async () => {
  try {
    await fastify.listen(3001)
    fastify.log.info(`server listening on ${fastify.server.address().port}`)
  } catch (err) {
    fastify.log.error(err)
    process.exit(1)
  }
}
start()
